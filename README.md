# 4R projekt Mood Lamp

vzorové repo pro odevzdání podkladů pro projekt Mood Lamp 4.R předmět IoT

## Obsah
* **otazky_prezentace.md**
  * otázky k prezentaci před třídou (splněno)
* **zadani.md**
  * přehled zadání minimálního a rozšířeného rozsahu 
* **dokumentace/fotky/**
  * nasdílejte alespoň 3 vhodné fotky (4 fotky ve složce dokumentace)
* **dokumentace/video/**
  * nasdílejte min. 1 minutové video, velikost do 50 MB (odkaz ve složce Video)
* (dokumentace/prezentace/ - volitelně, prezentace)
* **zdrojove_kody/** 
  * nasdílejte kompletní zdrojové kódy umožňující nahrání a spuštění projektu (ve složce zdrojové kódy)
* **zdrojove_kody/knihovny/** 
  * nasdílejte potřebné knihovny (ve složce zdrojové kódy)

## Postup
* proveďte fork tohoto repozitáře
* naplňte
* přidejte přístup / nasdílejte / požádejte o merge
